<?php

class Fattura24_AppFatturazione_Model_System_Config_Source_NatureIva
{

    public function toOptionArray(){
        $option = array(
            array('value' => "Scegli", 'label' => "Scegli..."),
            array('value' => "N1", 'label' => "N1 - escluse ex art.15 "),
            array('value' => "N2", 'label' => "N2 - non soggette "),
            array('value' => "N3", 'label' => "N3 - non imponibili "),
            array('value' => "N4", 'label' => "N4 - esenti"),
            array('value' => "N5", 'label' => "N5 - regime del margine / Iva non esposta in fattura "),
            array('value' => "N6", 'label' => "N6 - inversione contabile "),
            array('value' => "N7", 'label' => "N7 - IVA assolta in altro stato UE")
        );
        return $option;
    }
}
?>
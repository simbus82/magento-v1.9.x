<?php

class Fattura24_AppFatturazione_Model_System_Config_Source_CreaFattura
{
    public function toOptionArray()
    {
        $option = array(
            array('value' => 'd', 'label' => 'Disabilitata'),
            array('value' => 'f', 'label' => 'Fattura NON Elettronica'),
            array('value' => 'fe', 'label' => 'Fattura Elettronica'),
        );

        return $option;
    }
}
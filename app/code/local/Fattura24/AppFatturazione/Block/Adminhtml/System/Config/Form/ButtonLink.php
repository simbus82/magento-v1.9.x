<?php

class Fattura24_AppFatturazione_Block_Adminhtml_System_Config_Form_ButtonLink
    extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element) {
       
        $html = "<button>";
        $html .= "<a style='color:white;' href='https://www.app.fattura24.com/html/index.html' target='_blank'>";
        $html .= "Accedi a Fattura24";
        $html .= "</a>";
        $html .="</button>";
        
    
    return $html;

    }    
}
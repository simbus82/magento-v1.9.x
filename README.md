# [Modulo Fattura24 per Magento](http://www.fattura24.com/magento-modulo-fatturazione/)

Magento è un’eccellente piattaforma eCommerce che offre tutte le funzionalità necessarie ad avviare un’attività professionale di commercio elettronico. Il modulo offerto da [Fattura24](http://www.fattura24.com/) ne estende le funzionalità permettendoti di creare PDF professionali, analizzare l’andamento del tuo business e condividere tutto col il tuo commercialista.
Vediamo come installarlo e configurarlo.

### step 1 – preparazione
Per utilizzare Fattura24 come servizio di fatturazione in Magento ti occorre:

- un abbonamento Business attivo su Fattura24
- l’API KEY associata al tuo abbonamento Business. Per ottenerla vai in ‘Configurazione’->’App e Servizi esterni’, clicca sul pulsante ‘Configura’ di Magento e richiedi l’API KEY che ti sarà subito mostrata
- scaricare il modulo di Fattura24 per Magento

### step 2 – configurazione
Di seguito i passaggi da seguire per installare il modulo di Fattura24:

1. copia nella cartella di installazione di Magento il contenuto del file zip scaricato in questa pagina
2. dal pannello di amministrazione di Magento, vai su ‘System->Cache Management’. Clicca sui pulsanti in alto a destra ‘Flush Magento Cache’ e ‘Flush Cache Storage’
3. il modulo è attivato. Per configurarlo, dal pannello di amministrazione di Magento vai in ‘System’->’Configuration’ e seleziona le impostazioni di Fattura24.
4. inserisci nel campo ‘Api Key’ la tua API KEY di Fattura24
5. clicca su ‘Verifica API KEY’ per accertarti di aver inserito l’API KEY correttamente
6. spunta i campi che ti occorrono
7. per attivare il log dettagliato dei dati, utile per vedere i dati inviati a Fattura24 dall’e-commerce, assicurati che sia abilitato il log di Magento in ‘Sistema->Configurazione->Avanzate->Sviluppatore->Impostazioni log’. Il log verrà salvato in ‘var/log/fattura24.log’, ogni volta che il modulo di Fattura24 eseguirà un’operazione.

Per aggiornare il modulo alle versioni future occorre eseguire i punti 1,2.

Per disinstallare il modulo di Fattura24, accedi al database di Magento e digita il seguente codice SQL
```DELETE FROM core_resource WHERE code='appfatturazione_setup';```
La disinstallazione del modulo è necessaria qualora si volesse installare una versione precedente del modulo o per risolvere eventuali problemi di aggiornamento.

### step 3 – verifica e funzionamento
La configurazione si può considerare terminata.
Ogni qual volta riceverai un ordine sul tuo e-commerce, il modulo farà per conto tuo le seguenti operazioni:

- se hai spuntato ‘Salva cliente’ nelle impostazioni, aggiungerà il cliente nella rubrica di Fattura24 o ne aggiornerà i dati se già presente
- se hai spuntato ‘Crea ordine’, invierà a Fattura24 i dati dell’ordine
- se hai spuntato ‘Invia email’, verrà inviata un’email al cliente con il PDF dell’ordine

Quando clicchi sul pulsante ‘Crea fattura’ nella tabella degli ordini o quando crei la fattura all’interno dell’ordine, se hai spuntato ‘Crea fattura’ nelle impostazioni, il modulo eseguirà le seguenti operazioni:

- creerà in Fattura24 una ricevuta se il cliente non ha una Partita IVA, una fattura altrimenti
aggiungerà il cliente nella rubrica di Fattura24 o ne aggiornerà i dati se già presente
- creerà il PDF della ricevuta/fattura
- se hai spuntato ‘Scarica PDF’ scaricherà una copia del PDF dentro il tuo e-commerce
- se hai spuntato ‘Invia email’, verrà inviata un’email al cliente con il PDF della ricevuta/fattura
- se hai spuntato ‘Stato pagata’, la ricevuta/fattura sarà creata in Fattura24 direttamente nello stato ‘pagata’
- se hai spuntato ‘Disabilita ricevute’, verrà creata una fattura anziché una ricevuta anche in assenza della Partita IVA

Il modulo aggiunge tre pulsanti nella tabella degli ordini:

- il pulsante ‘Crea fattura’ serve per eseguire le azioni elencate sopra
- il pulsante ‘Scarica PDF’ serve per scaricare la ricevuta/fattura sul server di Magento. Se hai spuntato ‘Scarica PDF’ nelle impostazioni, questa verrà scaricata automaticamente quando viene creata
- il pulsante ‘Visualizza PDF’ serve per scaricare il PDF della ricevuta/fattura in locale

Il PDF della ricevuta/fattura può essere visualizzato anche dalla pagina dell’ordine sia dell’amministratore che del cliente.

Come hai visto utilizzare Fattura24 dentro Magento è semplicissimo ma se volessi assistenza tecnica non esitare a contattarci al numero +39 06-40402261.
Il nostro team tecnico è a tua completa disposizione per permetterti di ottenere il meglio dal tuo e-commerce e da Fattura24.
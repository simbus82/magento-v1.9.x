# Changelog
## 1.10.4
###### _Apr 16, 2019_
- Spostati link documentazione

## 1.10.3
###### _Apr 15, 2019_
- Aggiunto controllo Natura Iva predefinita per aliquote pari a 0% 

## 1.10.2
###### _Apr 10, 2019_
- Aggiunti controlli ai campi Pec e Codice destinatario

## 1.10.1
###### _Apr 8, 2019_
- Aggiunto pulsante per accedere al gestionale Fattura24 dalle impostazioni
- Aggiunti riferimenti normativi in calce alle impostazioni 

## 1.10.0
###### _Apr 3, 2019_ 
- Supporto fatturazione elettronica (beta)
- Aggiunti campi Sdi e PEC in checkout
- Aggiunto tipo di documento 'FE'

## 1.9.13
###### _Mag 5, 2018_ 
- risolto bug su ordini con regola di sconto
- risolto bug su invio dei prezzi errati con determinate configurazioni di Magento
- il campo "Oggetto" della ricevuta/fattura in Fattura24 ora viene valorizzato con il numero dell'ordine in Magento
- se il cliente non ha la Partita Iva ora vengono sempre presi il nome e cognome anziché il nome dell'azienda

## 1.9.12
###### _Dic 18, 2017_ 
- cambiato il comportamento dei pulsanti di visualizzazione fattura: se la fattura non è stata creata in Fattura24 o se è disabilitata la creazione delle fatture nella configurazione del modulo, allora viene visualizzato il PDF generato da Magento invece di mostrare un messaggio di errore

## 1.9.11
###### _Nov 16, 2017_ 
- aggiunta selezione sezionale per ricevute e fatture nelle impostazioni del modulo. Ora è possibile scegliere il sezionale dei documenti creati dagli e-commerce, cosicché si possono usare sezionali diversi per diversi e-commerce

## 1.9.10
###### _Nov 10, 2017_ 
- aggiunta selezione modello PDF per ordini e fatture con destinazio neelle impostazioni del modulo. Ora è possibile scegliere modelli diversi per i documenti a seconda che l'ordine contenga o meno l'indirizzo di spedizione del cliente
- aggiunta selezione conto nelle impostazioni del modulo. Ora è possibile scegliere il conto economico da associare alle prestazioni/prodotto dei documenti generati dall'e-commerce

## 1.9.9
###### _Set 14, 2017_

- aggiunto pieno supporto al multistore. Ora è possibile avere una configurazione del modulo diversa per ogni store
- rimossa checkbox 'Scarica PDF' relativa ai PDF delle fatture nella pagina di configurazione del modulo. Ora i PDF vengono sempre scaricati sul proprio e-commerce quando viene creata una fattura
- aggiunti alcuni messaggi di avviso nella pagina di configurazione del modulo

## 1.9.8
###### _Ago 04, 2017_ 

- aggiunta gestione magazzino nella pagina di configurazione del modulo. Ora è possibile decidere se gli ordini e/o le fatture movimentano il magazzino in Fattura24
- aggiunta selezione modello nella pagina di configurazione del modulo. Ora è possibile scegliere il modello con il quale vengono creati i PDF degli ordini e delle fatture

## 1.9.7 
###### _Lug 25, 2017_ 
 
- risolto bug su prezzo errato coupon
- risolto bug che avveniva su visualizzazione fattura da pannello di amministrazione quando questa non era stata scaricata

## 1.9.6
###### _Lug 19, 2017_

- risolto bug su prodotti con prezzo in catalogo comprensivo di iva
- risolto bug su calcolo errato dell’iva sui costi di spedizione
- risolto bug su prodotti configurabili
- aggiunta funzionalità di log dettagliato (per la configurazione, vedi lo step 1 del file README)

## 1.9.5
###### _Lug 14, 2017_

- risolto bug su nuova installazione del modulo

## 1.9.4
###### _Giu 28, 2017_

- aggiunta funzionalità per cui l'ordine impegna la merce nel magazzino di Fattura24 e la fattura la scarica se il prodotto ha lo stesso codice che ha sull'e-commerce

## 1.9.3
###### _Giu 21, 2017_

- aggiunto avviso ‘Nuova versione disponibile’ nella pagina di configurazione del modulo
- risolto bug minore

## 1.9.2
###### _Giu 9, 2017_

- abilitato invio del numero d’ordine a Fattura24

## 1.9.1
###### _Mag 23, 2017_

- risolto bug su url di reinderazzamento errato dei pulsanti nella tabella degli ordini